import fs from 'fs';
import path from 'path';
import { Stream } from 'stream';

const server: string = '//10.10.10.204';
const unidad: string = 'F';
const folder: string = 'SITES_IMP';
const subFolder: string = 'TEMIS_IMP';
const rutaIIS: string = path.join(server, unidad, folder, subFolder);
console.log(rutaIIS);

function getAllEnvironmentFromFolder(ruta: string) {

    let centrosCostos: string[];
    let rutasAmbientes: string[] = [];
    centrosCostos = fs.readdirSync(ruta);
    for (const ambiente of centrosCostos) {
        rutasAmbientes.push(path.join(ruta, ambiente));
    }
    return rutasAmbientes;
}

function readWebConfig(){
    let ambientes = getAllEnvironmentFromFolder(rutaIIS);
    if(ambientes && ambientes.length > 0){
        console.log(ambientes);
        
    }
}

readWebConfig()
//getAllEnvironmentFromFolder(rutaIIS);
